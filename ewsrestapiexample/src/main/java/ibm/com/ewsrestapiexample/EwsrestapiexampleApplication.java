package ibm.com.ewsrestapiexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EwsrestapiexampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(EwsrestapiexampleApplication.class, args);
	}

}
