package ibm.com.ewsrestapiexample.ews.basiccheck.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ibm.com.ewsrestapiexample.ews.basiccheck.model.Customer;

public interface BasicCheckRepository extends JpaRepository<Customer, Long> {

}
