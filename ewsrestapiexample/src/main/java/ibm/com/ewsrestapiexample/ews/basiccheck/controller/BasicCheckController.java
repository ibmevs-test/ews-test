package ibm.com.ewsrestapiexample.ews.basiccheck.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ibm.com.ewsrestapiexample.ews.basiccheck.model.Customer;
import ibm.com.ewsrestapiexample.ews.basiccheck.service.BasicCheckService;

@RestController
@RequestMapping("/ews/basiccheck")
public class BasicCheckController {
	
	@Autowired
	BasicCheckService basicCheckService;
	
	@PostMapping
	public void doBasicCheck(@RequestBody Customer customer) {
		basicCheckService.doBasicCheck(customer);
	}
	
	@GetMapping
	public List<Customer> list() {
		return basicCheckService.list();
	}

}
