package ibm.com.ewsrestapiexample.ews.basiccheck.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ibm.com.ewsrestapiexample.ews.basiccheck.model.Customer;
import ibm.com.ewsrestapiexample.ews.basiccheck.repository.BasicCheckRepository;

@Service
public class BasicCheckService {
	
	@Autowired
	BasicCheckRepository basicCheckRep;

	@Transactional
	public void doBasicCheck(Customer customer) {
		basicCheckRep.save(customer);		
	}

	public List<Customer> list() {
		return basicCheckRep.findAll();
	}

}
